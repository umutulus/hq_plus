using AutoMapper;
using Hangfire;
using HQPlus.Project.Api.Helper;
using HQPlus.Project.HttpInterceptor;
using HQPlus.Project.Model.View;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQPlus_Project
{
    public class Startup
    {

        private string _apiVersion;
        private string _apiTitle;
        private string _swaggerEndpoint;
        private string _swaggerTitle;
        private string _swaggerDocumentTitle;


        public static List<SearchModel> SearchModelForWeb;
        public static List<SearchModel> SearchModelForLocal;
        public static string JsonDataPath;
        public static string ExcelTemplatePath;
        public static string ExcelSavePath;
        public static string Task1Html;
        public static string CacheHost;
        public static int CacheExpiresMin;
        public static string ApiUrl;
        public static string ApiStatic;
        public static string ApiCallVersion;
        public static string ApiExcelReport;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            ReadConfiguration();

            FillSearchModelForWeb();

            FillSearchModelForLocal();
        }

        public IConfiguration Configuration { get; }

        public static void FillSearchModelForWeb()
        {
            SearchModelForWeb = new List<SearchModel>() {
            CreateModel(tag:"h2",attribute:"class",attributeValue:"hp__hotel-name",nodeName:"HotelName"),
            CreateModel(tag:"*",attribute:"class",attributeValue:"hp_address_subtitle",nodeName:"Address",isEqual:false),
            CreateModel(tag:"*",attribute:"class",attributeValue:"bui-rating",nodeName:"Star",isEqual:false,targetAttribute:"aria-label"),
            CreateModel(tag:"div",attribute:"class",attributeValue:"bui-review-score__badge",nodeName:"ReviewPoints",targetAttribute:"aria-label"),
            CreateModel(tag:"div",attribute:"class",attributeValue:"bui-review-score__text",nodeName:"NumberOfReview"),
            CreateModel(tag:"div",attribute:"id",attributeValue:"property_description_content",nodeName:"Description"),
            CreateModel(tag:"div",attribute:"class",attributeValue:"room-info",nodeName:"RoomTypes",isList:true)
            };
        }

        public static void FillSearchModelForLocal()
        {
            SearchModelForLocal = new List<SearchModel>()
            {
            CreateModel(tag:"span",attribute:"id",attributeValue:"hp_hotel_name",nodeName:"HotelName"),
            CreateModel(tag:"span",attribute:"id",attributeValue:"hp_address_subtitle",nodeName:"Address"),
            CreateModel(tag:"*",attribute:"class",attributeValue:"location_score_tooltip",nodeName:"Star"),
            CreateModel(tag:"span",attribute:"class",attributeValue:"rating notranslate",nodeName:"ReviewPoints"),
            CreateModel(tag:"*",attribute:"class",attributeValue:"count",nodeName:"NumberOfReview",targetAttribute:""),
            CreateModel(tag:"div",attribute:"id",attributeValue:"summary",nodeName:"Description",isEqual:true),
            CreateModel(tag:"td",attribute:"class",attributeValue:"ftd",nodeName:"RoomTypes",targetAttribute:"",isList:true),
            CreateModel(tag:"a",attribute:"class",attributeValue:"althotel_link",nodeName:"AltHotels",isList:true)
            };
        }

        private void ReadConfiguration()
        {

            _apiVersion = Configuration.GetValue<string>("Swagger:API:Version");
            _apiTitle = Configuration.GetValue<string>("Swagger:API:Title");
            ApiUrl= Configuration.GetValue<string>("Api:Url");
            ApiStatic = Configuration.GetValue<string>("Api:Api");
            ApiCallVersion = Configuration.GetValue<string>("Api:Version");
            ApiExcelReport = Configuration.GetValue<string>("Api:ExcelReport");

            _swaggerEndpoint = Configuration.GetValue<string>("Swagger:Endpoint");
            _swaggerTitle = Configuration.GetValue<string>("Swagger:Title");
            _swaggerDocumentTitle = Configuration.GetValue<string>($"Local:Swagger:DocumentTitle");

            JsonDataPath = Configuration.GetValue<string>($"LocalProjectFiles:Json");
            ExcelTemplatePath = Configuration.GetValue<string>($"LocalProjectFiles:Template");
            ExcelSavePath = Configuration.GetValue<string>($"LocalProjectFiles:SavePath");
            Task1Html = Configuration.GetValue<string>($"LocalProjectFiles:Task1Html");


            CacheHost = Configuration.GetValue<string>("Cache:REDISAddress");
            CacheExpiresMin = Configuration.GetValue<int>("Cache:ExpireMin");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();


            ConfigureDependencyInjection(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_apiVersion, new OpenApiInfo { Title = _apiTitle, Version = _apiVersion });
            });

            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddHttpClient();

            services.AddHangfire(configuration =>
            {
                configuration.UseRedisStorage(RedisConnectionFactory.Connection);
            });

        }


        public static async Task SendOnlineReports()
        {

          await  HttpCallHelper.CallAsync();


        }

        private static void ConfigureDependencyInjection(IServiceCollection services)
        {
            InjectionManager.Inject(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpMiddleware();

            app.UseRequestResponseLogging();

            app.UseCors(crs => crs.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseRouting();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(_swaggerEndpoint, _swaggerTitle);
                c.DocumentTitle = _swaggerDocumentTitle;
            });

            app.UseHangfireServer();

            app.UseHangfireDashboard("/hqplushangfire");


            RecurringJob.AddOrUpdate("send_online_reports", () => Startup.SendOnlineReports(), Cron.Daily);
        }

        private static SearchModel CreateModel(string nodeName, string tag, string attribute, string attributeValue, bool isEqual = true, string targetAttribute = "", bool isList = false)
        {
            return new SearchModel() { NodeName = nodeName, Tag = tag, Attribute = attribute, AttributeValue = attributeValue, IsEqual = isEqual, TargetAttribute = targetAttribute, IsList = isList };
        }
    }
}
