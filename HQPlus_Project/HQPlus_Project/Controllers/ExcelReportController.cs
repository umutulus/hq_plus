﻿using HQPlus.Project.Model.View;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HQPlus_Project;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace HQPlus.Project.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ExcelReportController : Controller
    {

        private readonly ILogger<ExcelReportController> _logger;
        private readonly Lazy<IExcelReportService> _excelReportService;

        public ExcelReportController(ILogger<ExcelReportController> logger, Lazy<IExcelReportService> excelReportService)
        {
            _logger = logger;
            _excelReportService = excelReportService;

        }

        [Route("create-rate-excel-report/{sendmail}")]
        [HttpGet]
        public async Task<ActionResult> CreateRateExcelReport(bool sendmail=false)
        {

            string jsonPath = $@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.JsonDataPath;
            string excelTemplatePath = $@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.ExcelTemplatePath;
            string excelSavedTemplatePath = $@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.ExcelSavePath + "_" + DateTimeOffset.Now.ToUnixTimeMilliseconds() + ".xlsx";


            string result = _excelReportService.Value.ExcelGenerate(jsonPath, excelTemplatePath, excelSavedTemplatePath) ? $"Please Check Your Local Folder --> {excelSavedTemplatePath}" : "Error Occurred";

            if (sendmail)
            {
                //SendMail
            }

            return StatusCode(StatusCodes.Status200OK, new ExcelExportResutViewModel { Result= result });
        }


    

    }
}
