﻿using HQPlus.Project.Api.Helper.Cache;
using HQPlus.Project.Model.Custom;
using HQPlus.Project.Model.Persistence;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HQPlus_Project;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace HQPlus.Project.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class HotelSearchController : BaseController
    {

        private readonly ILogger<DataExtractionController> _logger;
        private readonly Lazy<IHotelService> _hotelService;

        public HotelSearchController(ILogger<DataExtractionController> logger, Lazy<IHotelService> hotelService, RedisCache redisCache) : base(redisCache)
        {
            _logger = logger;
            _hotelService = hotelService;

        }

        [Route("search")]
        [HttpPost]
        public async Task<ActionResult> Search(HotelSearchPersistenceModel hotelSearchPersistenceModel)
        {

           await Startup.SendOnlineReports();
            string jsonPath = $@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.JsonDataPath;

            string cacheKey = (hotelSearchPersistenceModel.ArrivalDate.Date.Ticks + hotelSearchPersistenceModel.HotelID).ToString();
            if (_redisCache.Exists(cacheKey))
            {
                var cacheResult = _redisCache.Get<IEnumerable<HotelRate>>(cacheKey);
                return StatusCode(StatusCodes.Status200OK, cacheResult);
            }

            var searchResult = _hotelService.Value.HotelSearch(hotelSearchPersistenceModel.HotelID, hotelSearchPersistenceModel.ArrivalDate, jsonPath);
            _redisCache.Set(cacheKey, searchResult);
            return StatusCode(StatusCodes.Status200OK, searchResult);
        }

    }
}
