﻿using HQPlus.Project.Api.Helper.Cache;
using Microsoft.AspNetCore.Mvc;

namespace HQPlus.Project.Api.Controllers
{

    public class BaseController : Controller
    {
        public readonly RedisCache _redisCache;
        public BaseController(RedisCache redisCache)
        {
            _redisCache = redisCache;
        }
    }
}
