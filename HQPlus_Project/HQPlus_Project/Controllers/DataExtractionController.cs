﻿using HQPlus.Project.Api.Helper.Cache;
using HQPlus.Project.Model.Persistence;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HQPlus_Project;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQPlus.Project.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DataExtractionController : BaseController
    {
        private readonly Lazy<IDataExtractionService> _dataExtractionService;

        public DataExtractionController(Lazy<IDataExtractionService> dataExtractionService, RedisCache redisCache) :base(redisCache)
        {
            _dataExtractionService = dataExtractionService;
        }

     

        [Route("get-hotel-info")]
        [HttpPost]
        public async Task<ActionResult> GetHotelInfo(HotelInfoPersistenceModel hotelInfoPersistenceModel)
        {
     
            var isDefaultPage = string.IsNullOrEmpty(hotelInfoPersistenceModel.Url);

            string processUrl = isDefaultPage ? $@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.Task1Html : hotelInfoPersistenceModel.Url;

            if (!isDefaultPage)
            {
                if (_redisCache.Exists(processUrl))
                {
                    var cacheResult = _redisCache.Get<IDictionary<String, object>>(processUrl);
                    return StatusCode(StatusCodes.Status200OK, cacheResult);
                }

                var reaiTimeBookingSample = _dataExtractionService.Value.GetData(processUrl, Startup.SearchModelForWeb);
                _redisCache.Set(processUrl, reaiTimeBookingSample);
                return StatusCode(StatusCodes.Status200OK, reaiTimeBookingSample);
            }
            else
            {
                var localBookingSample = _dataExtractionService.Value.GetData(processUrl, Startup.SearchModelForLocal);
                return StatusCode(StatusCodes.Status200OK, localBookingSample);
            }

         
        }

    }
}
