﻿using HQPlus.Project.Api.Helper.Cache;
using HQPlus.Project.Service.AccessPoint.Classes;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace HQPlus.Project.Api.Helper
{
    public static class InjectionManager
    {
        private static void ServiceDependencies(IServiceCollection services)
        {
            services.AddScoped<IDataExtractionService, DataExtractionService>();
            services.AddScoped(provider => new Lazy<IDataExtractionService>(provider.GetService<IDataExtractionService>));

            services.AddScoped<IExcelReportService, ExcelReportService>();
            services.AddScoped(provider => new Lazy<IExcelReportService>(provider.GetService<IExcelReportService>));


            services.AddScoped<IHotelService, HotelService>();
            services.AddScoped(provider => new Lazy<IHotelService>(provider.GetService<IHotelService>));
        }

        private static void SettingsDependencies(IServiceCollection services)
        {
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        }

        private static void HelperDependencies(IServiceCollection services)
        {
            services.AddSingleton<HtmlWeb>();
            services.AddSingleton<RedisCache>();
        }

        public static void Inject(IServiceCollection services)
        {
            ServiceDependencies(services);
            SettingsDependencies(services);
            HelperDependencies(services);


        }
    }
}
