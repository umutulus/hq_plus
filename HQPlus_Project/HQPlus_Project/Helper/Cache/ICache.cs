﻿using System;

namespace HQPlus.Project.Api.Helper.Cache
{
    public interface ICache : IDisposable
    {
        T Get<T>(string key);

        void Set<T>(string key, T obj);

        void Delete(string key);

        bool Exists(string key);
    }
}
