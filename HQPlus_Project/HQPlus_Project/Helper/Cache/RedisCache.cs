﻿using HQPlus_Project;
using System;
using System.Text.Json;

namespace HQPlus.Project.Api.Helper.Cache
{
    public class RedisCache : ICache
    {
        private readonly StackExchange.Redis.IDatabase _redisDb;

        public RedisCache()
        {
            _redisDb = RedisConnectionFactory.Connection.GetDatabase();
        }

        public void Set<T>(string key, T objectToCache)
        {
            var expireTimeSpan = DateTime.Now.AddMinutes(Startup.CacheExpiresMin).Subtract(DateTime.Now);

            _redisDb.StringSet(key, JsonSerializer.Serialize(objectToCache), expireTimeSpan);
        }
        public T Get<T>(string key)
        {
            var redisObject = _redisDb.StringGet(key);

            return redisObject.HasValue ? JsonSerializer.Deserialize<T>(redisObject) : Activator.CreateInstance<T>();
        }
        public void Delete(string key)
        {
            _redisDb.KeyDelete(key);
        }
        public bool Exists(string key)
        {
            return _redisDb.KeyExists(key);
        }

        public void Dispose()
        {
            RedisConnectionFactory.Connection.Dispose();
        }
    }
}
