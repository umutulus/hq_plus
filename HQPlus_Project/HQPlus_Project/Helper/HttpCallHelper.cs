﻿using HQPlus_Project;
using System.Net.Http;
using System.Threading.Tasks;

namespace HQPlus.Project.Api.Helper
{
    public  class HttpCallHelper
    {
 
        public static async Task CallAsync()
        {
            using var client = new HttpClient();

            await client.GetAsync(Startup.ApiUrl + Startup.ApiStatic + Startup.ApiCallVersion + Startup.ApiExcelReport + "true");
        }
    }
}
