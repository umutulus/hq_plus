﻿using HQPlus.Project.Model.Custom;
using HQPlus.Project.Model.Persistence;
using HQPlus.Project.Service.AccessPoint.Classes;
using HQPlus.Project.Service.Helper;
using HQPlus_Project;
using HtmlAgilityPack;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace HQPlus.Project.Service.Test.Service
{
    [TestFixture]
    [Author("U.U.")]
    public class DataExtractionServiceTest
    {
        #region Variables

        private HtmlWeb _htmlWeb;
        private HotelInfoPersistenceModel _localLotelInfoPersistenceModel;
        private HotelInfoPersistenceModel _webLotelInfoPersistenceModel;

        #endregion


        #region DI_Inject
        private void AddServiceCollection()
        {
            var services = new ServiceCollection();
            services.AddSingleton<HtmlWeb>();
            var serviceProvider = services.BuildServiceProvider();
            _htmlWeb = serviceProvider.GetService<HtmlWeb>();
        }
        #endregion

        [OneTimeSetUp]
        public void Setup()
        {
            AddServiceCollection();
            Startup.Task1Html = "SampleFiles\\Task1\\task 1 - Kempinski Hotel Bristol Berlin, Germany - Booking.com.html";
            Startup.FillSearchModelForLocal();
            Startup.FillSearchModelForWeb();

        }



        [Test, Timeout(5000)]
        public void GetHotelInfoLocalTestAsync()
        {
            var dataExtractionService = new DataExtractionService(_htmlWeb);
            var result = dataExtractionService.GetData($@"{AppDomain.CurrentDomain.BaseDirectory}" + Startup.Task1Html, Startup.SearchModelForLocal);

            Assert.IsNotNull(result);
            Assert.That(result.ContainsKey("HotelName"));
            StringAssert.Contains("Kempinski",result["HotelName"].ToString());

        }


        [Test, Timeout(10000)]
        public void GetHotelInfoWebTestAsync()
        {
            var dataExtractionService = new DataExtractionService(_htmlWeb);
            var result =  dataExtractionService.GetData("https://www.booking.com/hotel/nl/luxer.tr.html", Startup.SearchModelForWeb);

            Assert.IsNotNull(result);
            Assert.That(result.ContainsKey("HotelName"));
            Assert.GreaterOrEqual((result["RoomTypes"] as List<TextHelper>).Count, 1);

        }


        [SetUp]
        public void ReInitializeTest()
        { }

        [TearDown]
        public void DisposeTest()
        {

        }

        [OneTimeTearDown]
        public void DisposeAllObjects()
        {

        }
    }
}
