﻿using HQPlus.Project.Api.Controllers;
using HQPlus.Project.Api.Helper.Cache;
using HQPlus.Project.Model.Persistence;
using HQPlus.Project.Service.AccessPoint.Classes;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HQPlus_Project;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace HQPlus.Project.Service.Test.Controller
{
    [TestFixture]
    [Author("U.U.")]
    public class DataExtractionControllerTest
    {
        #region Variables
        private HttpClient _client;
        private HtmlWeb _htmlWeb;
        private Lazy<IDataExtractionService> _dataExtractionService;
        private RedisCache _redisCache;
        private HotelInfoPersistenceModel _localLotelInfoPersistenceModel;
        private HotelInfoPersistenceModel _webLotelInfoPersistenceModel;
        private const string ServiceBaseURL = "http://localhost:5000/";
        #endregion

        #region DI_Inject
        private void AddServiceCollection()
        {
            var services = new ServiceCollection();


            services.AddTransient<IDataExtractionService, DataExtractionService>();
            services.AddTransient(provider => new Lazy<IDataExtractionService>(provider.GetService<IDataExtractionService>));
            services.AddSingleton<HtmlWeb>();
            services.AddSingleton<RedisCache>();

            var serviceProvider = services.BuildServiceProvider();

            _dataExtractionService = serviceProvider.GetService<Lazy<IDataExtractionService>>();
            _redisCache = serviceProvider.GetService<RedisCache>();
            _htmlWeb = serviceProvider.GetService<HtmlWeb>();
        }
        #endregion

        #region Test
        [OneTimeSetUp]
        public void Setup()
        {
            Startup.CacheHost = "redis-17268.c253.us-central1-1.gce.cloud.redislabs.com:17268,password=woI247v7fhys1rCeFgDQ6MCgmnA2f8Gz";
            Startup.CacheExpiresMin = 5;
            Startup.Task1Html = "SampleFiles\\Task1\\task 1 - Kempinski Hotel Bristol Berlin, Germany - Booking.com.html";
            Startup.FillSearchModelForLocal();
            Startup.FillSearchModelForWeb();

            AddServiceCollection();

            _client = new HttpClient { BaseAddress = new Uri(ServiceBaseURL) };
            _localLotelInfoPersistenceModel = new() { Url = null };
            _webLotelInfoPersistenceModel = new() { Url = "https://www.booking.com/hotel/nl/luxer.tr.html" };

        }


        [SetUp]
        public void ReInitializeTest()
        {
            _client = new HttpClient { BaseAddress = new Uri(ServiceBaseURL) };
        }


        [Test, Timeout(5000)]
        public async Task GetHotelInfoLocalTestAsync()
        {
            var dataExtractionController = new DataExtractionController(_dataExtractionService, _redisCache);
            var result = await dataExtractionController.GetHotelInfo(_localLotelInfoPersistenceModel);

            Assert.AreEqual((result as ObjectResult).StatusCode, 200);
            Assert.IsNotNull(result);
        }


        [Test, Timeout(10000)]
        public async Task GetHotelInfoWebTestAsync()
        {
            var dataExtractionController = new DataExtractionController(_dataExtractionService, _redisCache);

            var result = await dataExtractionController.GetHotelInfo(_webLotelInfoPersistenceModel);

            Assert.AreEqual((result as ObjectResult).StatusCode, 200);
            Assert.IsNotNull(result);

        }

        [OneTimeTearDown]
        public void DisposeAllObjects()
        {

            if (_client != null)
                _client.Dispose();
        }

        [TearDown]
        public void DisposeTest()
        {

            if (_client != null)
                _client.Dispose();
        } 
        #endregion

    }
}
