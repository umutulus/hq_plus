﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HQPlus.Project.Model.Persistence
{
    public class HotelSearchPersistenceModel
    {
        [Required]
        public int HotelID { get; set; }
        [Required]
        public DateTime ArrivalDate { get; set; }
    }
}
