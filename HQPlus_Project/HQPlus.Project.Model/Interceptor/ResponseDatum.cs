﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HQPlus.Project.Model.Interceptor
{
    public class ResponseDatum
    {
        public bool IsSuccessful { get; set; }

        public string Message { get; set; }

        public object Datum { get; set; }
    }
}
