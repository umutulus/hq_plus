﻿using System;

namespace HQPlus.Project.Model
{
    public class FriendlyAreaException : Exception
    {
        public FriendlyAreaException(string message) : base(message)
        {
        }
    }
}
