﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HQPlus.Project.Model.Base
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BaseViewModel
    {
        public virtual int Id { get; set; }
    }
}
