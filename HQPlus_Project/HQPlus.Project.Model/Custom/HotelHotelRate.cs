﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace HQPlus.Project.Model.Custom
{
    public class HotelHotelRate
    {
        [JsonPropertyName("hotel")]
        public Hotel Hotel { get; set; }

        [JsonPropertyName("hotelRates")]
        public List<HotelRate> HotelRates { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
    }
}
