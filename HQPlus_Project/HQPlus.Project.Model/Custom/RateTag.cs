﻿using System.Text.Json.Serialization;

namespace HQPlus.Project.Model.Custom
{
    public class RateTag
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("shape")]
        public bool Shape { get; set; }
    }
}
