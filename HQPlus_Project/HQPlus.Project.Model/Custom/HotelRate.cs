﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace HQPlus.Project.Model.Custom
{
    public class HotelRate
    {

        [JsonPropertyName("hotelID")]
        public int HotelID { get; set; }

        [JsonPropertyName("adults")]
        public int Adults { get; set; }

        [JsonPropertyName("los")]
        public int Los { get; set; }

        [JsonPropertyName("price")]
        public Price Price { get; set; }

        [JsonPropertyName("rateDescription")]
        public string RateDescription { get; set; }

        [JsonPropertyName("rateID")]
        public string RateID { get; set; }

        [JsonPropertyName("rateName")]
        public string RateName { get; set; }

        [JsonPropertyName("rateTags")]
        public List<RateTag> RateTags { get; set; }

        [JsonPropertyName("targetDay")]
        public DateTime TargetDay { get; set; }

        [JsonIgnore]
        public DateTime DepartureDate { get { return TargetDay.AddDays(Los); } }

        [JsonIgnore]
        public int BreakfastIncludeInteger { get { return Convert.ToBoolean(RateTags?.SingleOrDefault(a => a.Name == "breakfast")?.Shape) ? 1 : 0; } }


    }
}
