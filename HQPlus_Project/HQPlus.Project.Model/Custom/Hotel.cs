﻿using System.Text.Json.Serialization;

namespace HQPlus.Project.Model.Custom
{
    public class Hotel
    {
        [JsonPropertyName("hotelID")]
        public int HotelID { get; set; }

        [JsonPropertyName("classification")]
        public int Classification { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("reviewscore")]
        public double Reviewscore { get; set; }
    }
}
