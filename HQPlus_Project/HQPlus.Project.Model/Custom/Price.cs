﻿using System.Text.Json.Serialization;

namespace HQPlus.Project.Model.Custom
{
    public class Price
    {
        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("numericFloat")]
        public double NumericFloat { get; set; }

        [JsonPropertyName("numericInteger")]
        public int NumericInteger { get; set; }
    }
}
