﻿namespace HQPlus.Project.Model.View
{
    public class SearchModel
    {
        public string NodeName { get; set; }
        public string Tag { get; set; }
        public string Attribute { get; set; }
        public string AttributeValue { get; set; }
        public bool IsEqual { get; set; } = true;
        public string TargetAttribute { get; set; } = "";
        public bool IsList { get; set; } = false;
    }
}
