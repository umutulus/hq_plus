﻿using HQPlus.Project.Model;
using HQPlus.Project.Model.Interceptor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HQPlus.Project.HttpInterceptor
{
    public class HttpMiddleware
    {
        private readonly RequestDelegate _next;

        public HttpMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {

            if (context.Request.Path.HasValue && context.Request.Path.Value.Contains("swagger") || (context.Request.Path.HasValue && context.Request.Path.ToString().ToLower().Contains("hqplushangfire")))


            {
                try
                {
                    await _next.Invoke(context);
                }
                catch (Exception ex)
                {

                    throw;
                }

                return;
            }
            Stream originalBody = null;
            using (MemoryStream modifiedBody = new MemoryStream())
            {
                using (StreamReader streamReader = new StreamReader(modifiedBody))
                {
                    try
                    {
                        context.Response.OnStarting((state) =>
                        {
                            context.Response.ContentType = "application/json";

                            return Task.CompletedTask;
                        }, null);

                        originalBody = context.Response.Body;
                        context.Response.Body = modifiedBody;

                        await _next.Invoke(context);

                        modifiedBody.Seek(0, SeekOrigin.Begin);

                        string originalContent = streamReader.ReadToEnd();
                        context.Response.Body = originalBody;



                        if (context.Response.StatusCode != 204)
                        {
                            string newContent;

                            newContent = JsonConvert.SerializeObject(new ResponseDatum
                            {
                                Datum = JsonConvert.DeserializeObject(originalContent.Replace("\\n","")),
                                IsSuccessful = context.Response.StatusCode != 406
                            });
                            if (context.Response.StatusCode == 406)
                            {
                                context.Response.StatusCode = 200;
                            }

                            context.Response.ContentLength = Encoding.UTF8.GetBytes(newContent).Length;
                            await context.Response.WriteAsync(newContent);
                        }
                    }
                    catch (FriendlyAreaException friendlyAreaException)
                    {
                        if (!(originalBody is null) && !(modifiedBody is null))
                        {
                            modifiedBody?.Seek(0, SeekOrigin.Begin);
                            context.Response.Body = originalBody;
                        }
                        await HandleFriendlyAreaExceptionAsync(context, friendlyAreaException);
                    }
                    catch (UnauthorizedAccessException unauthorizedAccessException)
                    {
                        if (!(originalBody is null) && !(modifiedBody is null))
                        {
                            modifiedBody?.Seek(0, SeekOrigin.Begin);
                            context.Response.Body = originalBody;
                            context.Response.StatusCode = 401;
                        }
                    }
                    catch (Exception exception)
                    {
                        if (!(originalBody is null) && !(modifiedBody is null))
                        {
                            modifiedBody?.Seek(0, SeekOrigin.Begin);
                            context.Response.Body = originalBody;
                        }
                        await HandleExceptionAsync(context);
                        throw exception;
                    }
                }
            }
        }

        private async Task HandleExceptionAsync(HttpContext context)
        {
            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResponseDatum
            {
                Datum = null,
                IsSuccessful = false,
                Message = "Something Went Wrong!"
            }));
        }

        private async Task HandleFriendlyAreaExceptionAsync(HttpContext context, FriendlyAreaException friendlyAreaException)
        {
            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResponseDatum
            {
                Datum = null,
                IsSuccessful = false,
                Message = friendlyAreaException.Message
            }));
        }
    }

    public static class HttpMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpMiddleware>();
        }
    }
}
