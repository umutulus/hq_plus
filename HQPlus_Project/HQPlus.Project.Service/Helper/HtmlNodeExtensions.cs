﻿using HQPlus.Project.Model.View;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.Json.Serialization;

namespace HQPlus.Project.Service.Helper
{
    public static class HtmlNodeExtensions
    {
        public static IDictionary<String, object> DynamicSingleSearch(this HtmlNode htmlDocument, params SearchModel[] values)
        {
            dynamic expando = new ExpandoObject();
            var result = expando as IDictionary<String, object>;

            foreach (var value in values)
            {
                HtmlNodeCollection htmlNodes = htmlDocument.SelectNodes(
                 $"//{value.Tag}" + "[" + (value.IsEqual ? string.Empty : "contains(") + $"@{value.Attribute}" + (value.IsEqual ? "=" : ",") + $"'{value.AttributeValue}'" + (value.IsEqual ? string.Empty : ")") + "]");

                if (value.IsList)
                {
                    List<TextHelper> textHelpers = new ();
                    foreach (var item in htmlNodes)
                    {
                        textHelpers.Add(new() { Name = GetTargetValue(value, item) });
                    }

                    result[value.NodeName] = textHelpers;
                }
                else
                {
                    result[value.NodeName] = htmlNodes != null ? GetTargetValue(value, htmlNodes.FirstOrDefault()) : "";
                }


            }
            return result;
        }

        private static string GetTargetValue(SearchModel value, HtmlNode htmlNode)
        {
            return string.IsNullOrEmpty(value.TargetAttribute) ? htmlNode.InnerText : htmlNode.GetAttributeValue(value.TargetAttribute, "");
        }

    }

    public class TextHelper
    {
        [JsonPropertyName("Name")]
        public string Name { get; set; }
    }
}

