﻿using HQPlus.Project.Model.Custom;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQPlus.Project.Service.AccessPoint.Interfaces
{
    public interface IExcelReportService
    {
        bool ExcelGenerate(string jsonPath, string excelTemplatePath, string excelSavedTemplatePath);
        HotelHotelRate ReadRateFromJson(string jsonPath);
    }
}
