﻿using HQPlus.Project.Model.Custom;
using System;
using System.Collections.Generic;

namespace HQPlus.Project.Service.AccessPoint.Interfaces
{
    public interface IHotelService
    {
        IEnumerable<HotelRate> HotelSearch(int HotelId, DateTime ArrivalDate, string jsonPath);
    }
}
