﻿using HQPlus.Project.Model.View;
using System;
using System.Collections.Generic;

namespace HQPlus.Project.Service.AccessPoint.Interfaces
{
    public interface IDataExtractionService
    {
        IDictionary<String, object> GetData(string url, List<SearchModel> searchModels);
    }
}
