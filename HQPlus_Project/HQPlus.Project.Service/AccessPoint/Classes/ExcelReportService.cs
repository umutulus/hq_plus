﻿using ClosedXML.Report;
using HQPlus.Project.Model.Custom;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace HQPlus.Project.Service.AccessPoint.Classes
{
    public class ExcelReportService : IExcelReportService
    {

        public bool ExcelGenerate(string jsonPath, string excelTemplatePath, string excelSavedTemplatePath)
        {
            HotelHotelRate hotelHotelRate = ReadRateFromJson(jsonPath);
            hotelHotelRate.CreatedDate = DateTime.Now;
            return  CreateExcelViaTemplate(hotelHotelRate, excelSavedTemplatePath, excelTemplatePath);
        }


       public HotelHotelRate ReadRateFromJson(string jsonPath)
        {
            using StreamReader r = new StreamReader(jsonPath);
            string jsonResult = r.ReadToEnd();
            return JsonConvert.DeserializeObject<HotelHotelRate>(jsonResult);
        }

       private bool CreateExcelViaTemplate(HotelHotelRate sheet, string saveAsPath, string templatePath)
        {
            try
            {
                using (var template = new XLTemplate(templatePath))
                {
                    template.AddVariable(sheet);
                    template.Generate();
                    template.SaveAs(saveAsPath);
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }
               
        }


    }
}
