﻿using HQPlus.Project.Model.View;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using HQPlus.Project.Service.Helper;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace HQPlus.Project.Service.AccessPoint.Classes
{
    public class DataExtractionService : IDataExtractionService
    {
        //private readonly HttpClient _httpClient;
        private readonly HtmlWeb _htmlWeb;
        public DataExtractionService(/*IHttpClientFactory httpClientFactory,*/ HtmlWeb htmlWeb)
        {
            //_httpClient = httpClientFactory.CreateClient();
            _htmlWeb = htmlWeb;
        }

        public  IDictionary<String, object> GetData(string url,List<SearchModel> searchModels)
        {
            return FillWebValues(_htmlWeb.Load(url), searchModels);
        }

        public IDictionary<String, object> FillWebValues(HtmlDocument doc, List<SearchModel> searchModels)
        {
            return doc.DocumentNode.DynamicSingleSearch(
                searchModels.ToArray()
            );
        }
    }
}
