﻿using HQPlus.Project.Model.Custom;
using HQPlus.Project.Service.AccessPoint.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HQPlus.Project.Service.AccessPoint.Classes
{
    public class HotelService : IHotelService
    {
        private readonly Lazy<IExcelReportService> _excelReportService;
        public HotelService(Lazy<IExcelReportService> excelReportService)
        {
            _excelReportService = excelReportService;

        }
        public IEnumerable<HotelRate> HotelSearch(int HotelId, DateTime ArrivalDate,string jsonPath)
        {
            HotelHotelRate hotelHotelRate = _excelReportService.Value.ReadRateFromJson(jsonPath);

            return hotelHotelRate.HotelRates.Where(a => a.TargetDay.Date.Equals(ArrivalDate.Date) && a.HotelID.Equals(HotelId));
         
        }
    }
}
